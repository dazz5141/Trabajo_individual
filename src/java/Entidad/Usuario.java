
package Entidad;


public class Usuario {

   private int IdUsuario;
   private String Nombre;
   private String Apellido;
   private String user;
   private String pass;

    public Usuario() {
    }

    public Usuario(int IdUsuario) {
        this.IdUsuario = IdUsuario;
    }

    public Usuario(String user, String pass) {
        this.user = user;
        this.pass = pass;
    }

   
    public int getIdUsuario() {
        return IdUsuario;
    }

    public void setIdUsuario(int IdUsuario) {
        this.IdUsuario = IdUsuario;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getApellido() {
        return Apellido;
    }

    public void setApellido(String Apellido) {
        this.Apellido = Apellido;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    @Override
    public String toString() {
        return "Usuario{" + "IdUsuario=" + IdUsuario + ", Nombre=" + Nombre + ", Apellido=" + Apellido + ", user=" + user + ", pass=" + pass + '}';
    }
    
}
