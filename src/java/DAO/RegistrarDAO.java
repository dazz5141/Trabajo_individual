/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Entidad.Usuario;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author Dazz
 */
public class RegistrarDAO {
   
    private dbConexion conn;

    public RegistrarDAO(dbConexion conn) {
        this.conn = conn;
    }
    
    public boolean registrar(Usuario usuario){
        PreparedStatement registrarUsuario;
        
        try{
            registrarUsuario = conn.getConnection().prepareStatement(
                    "INSERT INTO usuario (Nombre, Apellido, user, pass)"
                            + "VALUES (?,?,?,?)");
            registrarUsuario.setString(1, usuario.getNombre());
            registrarUsuario.setString(2, usuario.getApellido());
            registrarUsuario.setString(3, usuario.getUser());
            registrarUsuario.setString(4, usuario.getPass());

            
            registrarUsuario.executeUpdate();
            
            return true;
        } catch (SQLException e){
            System.out.println("Error: "+e);
            return false;
       
        }
    }
    
}
