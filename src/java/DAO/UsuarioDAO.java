/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Entidad.Usuario;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author PtoMontt_B305
 */
public class UsuarioDAO {
    
    private dbConexion conn;

    public UsuarioDAO(dbConexion conn) {
        this.conn = conn;
    }
    
    public Usuario login(String us, String contra) {

        try {
            PreparedStatement preparedStatement = conn.getConnection().prepareStatement("select * from usuario where user=? and pass= ?");
            preparedStatement.setString(1, us);
            preparedStatement.setString(2, contra);
            ResultSet rs = preparedStatement.executeQuery();
            Usuario usua = new Usuario(0);
            while (rs.next()) {
                usua.setIdUsuario(rs.getInt("idUsuario"));
                usua.setUser(rs.getString("user"));
                usua.setPass(rs.getString("pass"));
            }
            return usua;
        } catch (SQLException e) { 
            System.out.println("Error: " + e.getMessage());
            return null;
        }
    }

    
}
