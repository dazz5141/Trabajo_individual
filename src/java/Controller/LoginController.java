/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DAO.UsuarioDAO;
import DAO.dbConexion;
import Entidad.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Dazz
 */
public class LoginController extends HttpServlet {

   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
    }

    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
            
        HttpSession session = request.getSession();
        RequestDispatcher rd;
        if (session.getAttribute("usuario") == null) {
            request.getRequestDispatcher("/login.jsp").forward(request, response);
        }
        
        
    }

    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        String user,contra;
        user= request.getParameter("nick");
        contra= request.getParameter("password");
        dbConexion conn = new dbConexion();
        UsuarioDAO usDa = new UsuarioDAO(conn);
        Usuario usuario = usDa.login(user, contra);
        conn.disconnect();
        if(usuario.getIdUsuario()>0){
                Usuario usu= new Usuario(user, contra);
                HttpSession session = request.getSession();
                session.setAttribute("usuario", usu);
                request.getRequestDispatcher("/Registrar.jsp").forward(request, response);
        }else{         
            request.getRequestDispatcher("/login.jsp").forward(request, response);
        }  
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
