/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DAO.RegistrarDAO;
import DAO.dbConexion;
import Entidad.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Dazz
 */
public class UsuarioRegistrarController extends HttpServlet {

    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
    }

   
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        HttpSession session = request.getSession();
        String nom, ape, nick, pass;
        nom=request.getParameter("Nombre");
        ape=request.getParameter("Apellido");
        nick=request.getParameter("user");
        pass=request.getParameter("password");
        
        dbConexion conect = new dbConexion();
         RegistrarDAO regD = new RegistrarDAO(conect);
        Usuario usua = new Usuario();
        
        if(session.getAttribute("usuario")==null){
            request.getRequestDispatcher("/login.jsp").forward(request, response);
        }else{
            usua.setNombre(nom);
            usua.setApellido(ape);
            usua.setUser(nick);
            usua.setPass(pass);
           
            
            regD.registrar(usua);
            conect.disconnect();
            request.getRequestDispatcher("/RegistroExitoso.jsp").forward(request, response);
        }
        
        
        
        
    }

    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
